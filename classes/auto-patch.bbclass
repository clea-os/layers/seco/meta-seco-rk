# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

CURDIR := "${THISDIR}/"
PATCHPATH ?= "${@d.getVar('CURDIR') + d.getVar('BPN') + '_' + d.getVar('PV')}"

python () {
    dirs = d.getVar('PATCHPATH') or ''
    for dir in dirs.split():
        if not os.path.isdir(dir):
            bb.warn("Patch source folder does not exist: " + dir)
            continue
        
        bb.debug(2, 'Using patch fodler: ' + dir)
        bb.parse.mark_dependency(d, dir)

        files = os.listdir(dir)
        files.sort()
        for file in files:
            if file.endswith('.patch'):
                d.appendVar('SRC_URI', ' file://' + dir + '/' + file)
                bb.debug(2, 'Adding patch: ' + file + ' for ' + dir)
}
