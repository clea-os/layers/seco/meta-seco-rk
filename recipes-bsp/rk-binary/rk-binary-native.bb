# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

inherit freeze-rev local-git deploy native

DESCRIPTION = "Rockchip binary tools"

LICENSE = "LICENSE.rockchip"
LIC_FILES_CHKSUM = "file://${RKBASE}/licenses/LICENSE.rockchip;md5=d63890e209bf038f44e708bbb13e4ed9"


RKBIN_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/tools/rkbin.git;name=rkbin;"
RKTOOLS_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/tools/rk-tools.git;name=tools;destsuffix=git/extra;"
PROTOCOL ?= "protocol=https;"
REPO_USER ?= ""
SRCBRANCH_RKBIN = "seco_4.19"
SRCBRANCH_RKTOOLS = "main"
SRC_URI_RKBIN = "${RKBIN_SRC}branch=${SRCBRANCH_RKBIN};${PROTOCOL}${REPO_USER}"
SRC_URI_RKTOOLS = "${RKTOOLS_SRC}branch=${SRCBRANCH_RKTOOLS};${PROTOCOL}${REPO_USER}"
SRC_URI = " \
	${SRC_URI_RKBIN} \
	${SRC_URI_RKTOOLS} \
"




SRCREV_rkbin = "320f05a1f5cdfceb24d55119ad05148a11895b2a"
SRCREV_tools = "f4425b92a2bb987260a4269637048106400a5c36"
SRCREV_FORMAT ?= "rkbin_tools"

S = "${WORKDIR}/git"

INSANE_SKIP:${PN} = "already-stripped"

# The pre-built tools have different link loader, don't change them.
UNINATIVE_LOADER := ""

do_install () {
	install -d ${D}/${bindir}

	cd ${S}/tools

	install -m 0755 boot_merger ${D}/${bindir}
	install -m 0755 trust_merger ${D}/${bindir}
	install -m 0755 firmwareMerger ${D}/${bindir}

	install -m 0755 kernelimage ${D}/${bindir}
	install -m 0755 loaderimage ${D}/${bindir}

	install -m 0755 mkkrnlimg ${D}/${bindir}
	install -m 0755 resource_tool ${D}/${bindir}

	install -m 0755 upgrade_tool ${D}/${bindir}

	cd ${S}/extra/linux/Linux_Pack_Firmware/rockdev

	install -m 0755 afptool ${D}/${bindir}
	install -m 0755 rkImageMaker ${D}/${bindir}
}

NATIVE_TOOLS = "boot_merger trust_merger firmwareMerger loaderimage mkkrnlimg resource_tool upgrade_tool"
NATIVE_EXTRA_TOOLS = "afptool rkImageMaker"

addtask deploy before do_build after do_compile
do_deploy () {
	cd ${S}/tools
	
	if [ ! -e ${DEPLOY_DIR_IMAGE}/rk_tools ]; then
		install -d ${DEPLOY_DIR_IMAGE}/rk_tools
	fi

	for binary in "${NATIVE_TOOLS}"; do
		if [[ ! -e ${DEPLOY_DIR_IMAGE}/rk_tools/${binary} ]]; then
			install -m 0755 ${binary}  ${DEPLOY_DIR_IMAGE}/rk_tools
		fi
	done

	cd ${S}/extra/linux/Linux_Pack_Firmware/rockdev

	for binary in "${NATIVE_EXTRA_TOOLS}"; do
		if [[ ! -e ${DEPLOY_DIR_IMAGE}/rk_tools/${binary} ]]; then
			install -m 0755 ${binary}  ${DEPLOY_DIR_IMAGE}/rk_tools
		fi
	done
}
