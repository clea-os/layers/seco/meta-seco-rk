HOMEPAGE = "http://www.denx.de/wiki/U-Boot/WebHome"
SECTION = "bootloaders"
DEPENDS += "rk-binary-native coreutils-native flex-native bison-native dtc-native"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"
do_configure[cleandirs] = "${B}"
PE = "1"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"


UBOOT_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/u-boot-seco-rk.git;"
PROTOCOL ?= "protocol=https;"

REPO_USER ?= ""
SRCBRANCH ??= "seco_2017.09"
SRC_URI = "${UBOOT_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"
SRCREV = "${AUTOREV}"

def sanitize_override(s):
    """
    Return s as a string compatible with the bitbake override syntax;
    all characters different than [a-z0-9-_] are replaced with an underscore.
    """
    import re
    return re.sub("[^a-z0-9-_]+", "_", s)

OVERRIDES:append = ":u-boot-${@sanitize_override(d.getVar('SRCBRANCH'))}"
