require u-boot-seco-rk-common.inc

SUMMARY = "U-Boot bootloader SYSDATA utilities"
DEPENDS += "mtd-utils flex-native bison-native"

PROVIDES += "fw-sysdata"
RPROVIDES:${PN} = "fw-sysdata"

INSANE_SKIP:${PN} = "already-stripped"
EXTRA_OEMAKE:class-target = 'CROSS_COMPILE=${TARGET_PREFIX} CC="${CC} ${CFLAGS} ${LDFLAGS}" HOSTCC="${BUILD_CC} ${BUILD_CFLAGS} ${BUILD_LDFLAGS}" V=1'
EXTRA_OEMAKE:class-cross = 'HOSTCC="${CC} ${CFLAGS} ${LDFLAGS}" V=1'

inherit uboot-config


do_compile () {
	oe_runmake -C ${S} O=${B} ${UBOOT_MACHINE}
	oe_runmake -C ${S} O=${B} sysdatatools
}

do_install () {
	install -d ${D}${base_sbindir}
	install -m 755 ${B}/tools/sysdata/fw_sysdata ${D}${base_sbindir}
}

do_install_class-cross () {
	install -d ${D}${bindir_cross}
	install -m 755 ${B}/tools/sysdata/fw_sysdata ${D}${bindir_cross}/fw_sysdata
}

SYSROOT_DIRS:append_class-cross = " ${bindir_cross}"

PACKAGE_ARCH = "${MACHINE_ARCH}"
BBCLASSEXTEND = "cross"
