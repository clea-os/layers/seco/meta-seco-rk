require u-boot-seco-rk-common.inc
require recipes-bsp/u-boot/u-boot.inc

PV = "2017.09+git${SRCPV}"
SRCBRANCH ?= "seco_2017.09"

inherit freeze-rev
inherit auto-patch

PATCHPATH = "${CURDIR}/u-boot-seco-rk"
FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot-scr-files:"

# Force using python2 for BSP u-boot
DEPENDS += "python-native u-boot-mkimage-native"
EXTRA_OEMAKE += "PYTHON=nativepython"

# Make sure we use nativepython
do_configure:prepend() {
	for s in `grep -rIl python ${S}`; do
		sed -i -e '1s|^#!.*python[23]*|#!/usr/bin/env nativepython|' $s
	done
}

#Generate rockchip style u-boot binary
UBOOT_BINARY = "uboot.img"
do_compile:append () {
	UBOOT_TEXT_BASE=$(grep -w "CONFIG_SYS_TEXT_BASE" ${B}/include/autoconf.mk)
	loaderimage --pack --uboot ${B}/u-boot.bin ${B}/${UBOOT_BINARY} \
            ${UBOOT_TEXT_BASE#*=} --size "${RK_LOADER_SIZE}" "${RK_LOADER_BACKUP_NUM}"
}
