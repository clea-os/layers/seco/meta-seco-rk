require u-boot-seco-rk-common.inc
require recipes-bsp/u-boot/u-boot.inc

PV = "RK3399.2017.09"

inherit auto-patch freeze-rev python3-dir

#PATCHPATH = "${CURDIR}/u-boot-seco-rk"
FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot-seco-rk:"

SRCBRANCH ?= "seco_2017.09_next"

RKBIN_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/tools/rkbin.git;name=rkbin;"
SRCBRANCH_rkbin = "seco_5.10"
SRCREV_rkbin = "5a9f2296ee98303abae806f307079eea53b47015"
SRC_URI = " \
        ${UBOOT_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER} \
        ${RKBIN_SRC}branch=${SRCBRANCH_rkbin};${PROTOCOL}${REPO_USER};destsuffix=rkbin; \
"
SRC_URI:append = " \
       file://0002-Revert-Makefile-enable-Werror-option.patch \
       file://0007-DTS-MAKEFILE-Remove-compilation-of-all-dtb.patch \
"

SRCREV_FORMAT = "default_rkbin"

DEPENDS:append = " ${PYTHON_PN}-native"

# Needed for packing BSP u-boot
DEPENDS:append = " bc-native coreutils-native ${PYTHON_PN}-pyelftools-native"

LICENSE:append = " & LICENSE.rockchip"
LIC_FILES_CHKSUM:append = " file://${RKBASE}/licenses/LICENSE.rockchip;md5=d63890e209bf038f44e708bbb13e4ed9"

do_configure:prepend() {
	# Make sure we use /usr/bin/env ${PYTHON_PN} for scripts
	for s in `grep -rIl python ${S}`; do
		sed -i -e '1s|^#!.*python[23]*|#!/usr/bin/env ${PYTHON_PN}|' $s
	done

	# Support python3
	sed -i -e 's/\(open(.*[^"]\))/\1, "rb")/' -e 's/,$//' \
		-e 's/print >> \([^,]*\), *\(.*\)$/print(\2, file=\1)/' \
		-e 's/print \(.*\)$/print(\1)/' \
		${S}/arch/arm/mach-rockchip/make_fit_atf.py

	# Remove unneeded stages from make.sh
	sed -i -e 's/^select_toolchain$//g' -e '/^clean/d' -e '/^\t*make/d' ${S}/make.sh
	
	# The latest version of boot_merger does not support the replace option 
	../rkbin/tools/boot_merger --help | grep "replace" || echo "this boot_merger's version does not support --replace option" \
	&& sed -i -e 's/^BIN_PATH_FIXUP.*/BIN_PATH_FIXUP=""/g' ${S}/make.sh

	# Prevent shell compatibility issue
	sed -e '/at the end of the file/a pound := \\\#' -e "s|echo '\\\#include|echo '\$\(pound\)include|g" -i "${S}/scripts/Makefile.lib"

	if [ "x${RK_ALLOW_PREBUILT_UBOOT}" = "x1" ]; then
		# Copy prebuilt images
		if [ -e "${S}/${UBOOT_BINARY}" ]; then
			bbnote "${PN}: Found prebuilt images."
			mkdir -p ${B}/prebuilt/
			mv ${S}/*.bin ${S}/*.img ${B}/prebuilt/
		fi
	fi

	[ -e "${S}/.config" ] && make -C ${S} mrproper
}

# Generate Rockchip style loader binaries
RK_IDBLOCK_IMG = "idblock.img"
RK_LOADER_BIN = "loader.bin"
RK_TRUST_IMG = "trust.img"
UBOOT_BINARY = "uboot.img"

do_compile:append() {
	cd ${B}

	# Modify rk3399_ddr_933MHz_v*.bin file using ddrbin_param.txt.
	sed -i 's/^uart baudrate=.*$/uart baudrate=115200/' ../rkbin/tools/ddrbin_param.txt
	../rkbin/tools/ddrbin_tool ../rkbin/tools/ddrbin_param.txt ../rkbin/bin/rk33/rk3399_ddr_933MHz_v*.bin

	if [ -e "${B}/prebuilt/${UBOOT_BINARY}" ]; then
		bbnote "${PN}: Using prebuilt images."
		ln -sf ${B}/prebuilt/*.bin ${B}/prebuilt/*.img ${B}/
	else
		# Prepare needed files
		for d in make.sh scripts configs arch/arm/mach-rockchip; do
			cp -rT ${S}/${d} ${d}
		done

		# Pack rockchip loader images
		./make.sh
	fi

	ln -sf *_loader*.bin "${RK_LOADER_BIN}"

	# Generate idblock image
	bbnote "${PN}: Generating ${RK_IDBLOCK_IMG} from ${RK_LOADER_BIN}"
	./tools/boot_merger --unpack "${RK_LOADER_BIN}"

	if [ -f FlashHead ];then
		cat FlashHead FlashData > "${RK_IDBLOCK_IMG}"
	else
		./tools/mkimage -n "${SOC_FAMILY}" -T rksd -d FlashData \
			"${RK_IDBLOCK_IMG}"
	fi

	cat FlashBoot >> "${RK_IDBLOCK_IMG}"
}

do_deploy:append() {
	cd ${B}

	for binary in "${RK_IDBLOCK_IMG}" "${RK_LOADER_BIN}" "${RK_TRUST_IMG}";do
		[ -f "${binary}" ] || continue
		install "${binary}" "${DEPLOYDIR}/${binary}-${PV}"
		ln -sf "${binary}-${PV}" "${DEPLOYDIR}/${binary}"
	done
	install -m 0755 *_loader*.bin ${DEPLOYDIR}
}
