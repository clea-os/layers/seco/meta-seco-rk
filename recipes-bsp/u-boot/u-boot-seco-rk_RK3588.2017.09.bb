require u-boot-seco-rk-common.inc
require recipes-bsp/u-boot/u-boot.inc

PV = "RK3588.2017.09"

inherit freeze-rev auto-patch

PATCHPATH = ""
FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot-scr-files:"

# Force using python2 for BSP u-boot
DEPENDS += "python-native u-boot-mkimage-native"
EXTRA_OEMAKE += "PYTHON=nativepython"

SRCREV = "${AUTOREV}"
SRCBRANCH ?= "seco_2017.09_next"

RKBIN_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/tools/rkbin.git;name=rkbin;"

SRCBRANCH_rkbin = "seco_5.10_rk3588"
SRCREV_rkbin = "${AUTOREV}"
SRC_URI = " \
	${UBOOT_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER} \
	${RKBIN_SRC}branch=${SRCBRANCH_rkbin};${PROTOCOL}${REPO_USER};destsuffix=rkbin; \
"
SRCREV_FORMAT = "default_rkbin"

SRC_URI += " \
    file://0002-Revert-Makefile-enable-Werror-option.patch \
    file://0007-DTS-MAKEFILE-Remove-compilation-of-all-dtb.patch \
"

# Make sure we use nativepython
do_configure:prepend() {
	for s in `grep -rIl python ${S}`; do
		sed -i -e '1s|^#!.*python[23]*|#!/usr/bin/env nativepython|' $s
	done
	
	# Modify rk3588_ddr_lp4_2112MHz_lp5_2736MHz_v*.bin file using ddrbin_param.txt.
	sed -i 's/uart baudrate=/uart baudrate=115200/' ../rkbin/tools/ddrbin_param.txt
	../rkbin/tools/ddrbin_tool ../rkbin/tools/ddrbin_param.txt ../rkbin/bin/rk35/rk3588_ddr_lp4_2112MHz_lp5_2736MHz_v*.bin
}

#Generate rockchip style u-boot binary
RK_IDBLOCK_IMG = "idblock.img"
RK_LOADER_BIN = "loader.bin"
RK_TRUST_IMG = "trust.img"
UBOOT_BINARY = "uboot.img"

do_compile:append () {
	cd ${B}

    #bbnote "${PN}: Generating ${RK_IDBLOCK_IMG} from ${FLASH_DATA} and ${FLASH_BOOT} for ${SOC_FAMILY}"
    if [ -e "${B}/prebuilt/${UBOOT_BINARY}" ]; then
        bbnote "${PN}: Using prebuilt images."
        ln -sf ${B}/prebuilt/*.bin ${B}/prebuilt/*.img ${B}/
    else
        # Prepare needed files
        for d in make.sh scripts configs arch/arm/mach-rockchip; do
            cp -rT ${S}/${d} ${d}
        done

        # Pack rockchip loader images
        ./make.sh 
    fi

	ln -sf *_loader*.bin "${RK_LOADER_BIN}"

    # Generate idblock image
	bbnote "${PN}: Generating ${RK_IDBLOCK_IMG} from ${RK_LOADER_BIN}"
	./tools/boot_merger --unpack "${RK_LOADER_BIN}"


	if [ -f FlashHead ];then
		cat FlashHead FlashData > "${RK_IDBLOCK_IMG}"
	else
        mkimage -n "${SOC_FAMILY}" -T rksd -d "${FLASH_DATA}" "${RK_IDBLOCK_IMG}"
	fi
	
    if [ -f FlashHead ];then
        cat FlashHead FlashData > "${RK_IDBLOCK_IMG}"
    else
        ./tools/mkimage -n "${SOC_FAMILY}" -T rksd -d FlashData "${RK_IDBLOCK_IMG}"
    fi

    cat FlashBoot >> "${RK_IDBLOCK_IMG}"
}

inherit deploy

do_deploy:append () {
    bbnote "${PN}: ${PWD} --- ${DEPLOYDIR}"
    install "${B}/${RK_LOADER_BIN}" "${DEPLOYDIR}/${RK_LOADER_BIN}"
    install "${B}/${RK_IDBLOCK_IMG}" "${DEPLOYDIR}/${RK_IDBLOCK_IMG}"
}
