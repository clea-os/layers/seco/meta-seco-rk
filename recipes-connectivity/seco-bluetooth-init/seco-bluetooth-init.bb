DESCRIPTION = "SECO Bluetooth Init Service"
HOMEPAGE = "https://www.seco.com"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit systemd

RDEPENDS:${PN} += "bash"

SYSTEMD_SERVICE:${PN} = "seco-bluetooth-init.service"
SYSTEMD_AUTO_ENABLE = "enable"

FILESEXTRAPATHS:prepend = "${THISDIR}/files:"

SRC_URI = " \
    file://seco-bluetooth-init.service \
    file://seco-bluetooth-init \
"

do_install:append() {
    install -d ${D}${systemd_unitdir}/system
    install -d ${D}/usr/bin

    install -m 0644 ${WORKDIR}/seco-bluetooth-init.service ${D}${systemd_unitdir}/system
    install -D -m 0777 ${WORKDIR}/seco-bluetooth-init ${D}/usr/bin/
}

FILES:${PN} = " \
    ${systemd_unitdir}/system/seco-bluetooth-init.service \
    /usr/bin/seco-bluetooth-init \
"
