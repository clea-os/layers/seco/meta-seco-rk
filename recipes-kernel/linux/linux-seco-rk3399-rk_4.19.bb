require recipes-kernel/linux/linux-yocto.inc
require linux-seco-rk.inc

inherit freeze-rev

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

KERNEL_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/linux-seco-rk.git;"
PROTOCOL ?= "protocol=https;"
REPO_USER ?= ""
SRCBRANCH = "seco_4.19.111"
SRC_URI = "${KERNEL_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"
SRCREV = "${AUTOREV}"

KERNEL_VERSION_SANITY_SKIP = "1"
LINUX_VERSION ?= "4.19"