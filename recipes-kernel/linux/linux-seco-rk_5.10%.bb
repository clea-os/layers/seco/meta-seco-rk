require recipes-kernel/linux/linux-yocto.inc
require linux-seco-rk.inc

inherit freeze-rev

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"


KERNEL_SRC ?= "git://git.seco.com/clea-os/bsp/rockchip/linux-seco-rk.git;"
PROTOCOL ?= "protocol=https;"
REPO_USER ?= ""
SRCBRANCH = "seco_5.10.110"
SRC_URI = "${KERNEL_SRC}branch=${SRCBRANCH};${PROTOCOL}${REPO_USER}"
SRCREV = "${AUTOREV}"

PV = "5.10"

KERNEL_VERSION_SANITY_SKIP = "1"
LINUX_VERSION ?= "5.10"


SRC_URI:append = " \
    file://docker.cfg \
"

SRC_URI:append:rk3588 = " \
    file://mali-csf.cfg \
"
